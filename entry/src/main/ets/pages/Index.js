import display from '@ohos.display';
import buttonPicDesc from '../blocks/components/buttonPicDesc';
import web_webview from '@ohos.web.webview';
struct Index extends   {
    constructor() { }
    aboutToAppear() {
        if (this.ScreenWv != 0) {
            if (this.ScreenWv / 600 <= 1) {
                this.BottomWidth = this.ScreenWv * 0.95;
            }
            else {
                this.BottomWidth = 340;
                this.LargeScreen = true;
            }
        } //For real devices: Decide Bottom width
        else {
            this.ScreenWv = 360;
            this.ScreenHv = 780;
            this.LargeScreen = false;
        } //For Previewer
        for (let index = 0; index < 10; index++) {
            this.tabs.push(index);
        }
        console.log(this.tabs.length.toString());
        //fill tabs[]
        for (let index = 0; index < this.tabs.length; index++) {
            this.webControllers.push(new web_webview.WebviewController());
        }
        console.log(this.webControllers.length.toString());
        //fill webControllers[]
        for (let index = 0; index < this.tabs.length; index++) {
            this.urls.push("");
        }
        console.log(this.urls.length.toString());
        //fill urls[]
    }
    onBackPress() {
        if (this.BottomUpFlag == true) {
            this.BottomUpFlag = false;
            this.BottomHeight = 55;
        }
        if (this.TabsUpFlag == true) {
            this.TabsUpFlag = false;
            this.TabsHeight = 55;
        }
        return true;
    }
    build() {
            .width("100%")
            .alignContent(Alignment.Bottom)
            .backgroundColor($r("app.color.LinysBG"));
    }
}
//# sourceMappingURL=Index.js.map