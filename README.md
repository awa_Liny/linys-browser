# LinysBrowser

#### 介绍
这是一个基于 HarmonyOS 的浏览器。

#### 软件架构
目前基于 API 9


#### 安装教程

1.  :O

#### 使用说明

1.  目前发现一个 .backgroundBlur() 在 webView 上失效的问题；
2.  目前发现一个来回切换标签页会爆炸的问题；

#### 参与贡献

1.  感谢九弓子老师！

#### 特技

1.  DUANG
